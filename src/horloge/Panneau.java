/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horloge;

import g40955.util.*;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
 
public class Panneau extends JPanel { 
    @Override
    public void paintComponent(Graphics g){
        int heureB, minuteB, secondeB;
        String heureH, minuteH, secondeH;
        int heureO, minuteO, secondeO;
        heureB = Conversion.Dec_Bin(Time.ThisHourH(Time.ThisHour()));
        minuteB = Conversion.Dec_Bin(Time.ThisHourM(Time.ThisHour()));
        secondeB = Conversion.Dec_Bin(Time.ThisHourS(Time.ThisHour()));
        heureH = Conversion.Dec_Hex(Time.ThisHourH(Time.ThisHour()));
        minuteH = Conversion.Dec_Hex(Time.ThisHourM(Time.ThisHour()));
        secondeH = Conversion.Dec_Hex(Time.ThisHourS(Time.ThisHour()));
        heureO = Conversion.Dec_Oct(Time.ThisHourH(Time.ThisHour()));
        minuteO = Conversion.Dec_Oct(Time.ThisHourM(Time.ThisHour()));
        secondeO = Conversion.Dec_Oct(Time.ThisHourS(Time.ThisHour()));
        
        //Instanciation d'un objet JPanel
        JPanel pan = new JPanel();
        Font font = new Font("Courier", Font.BOLD, 60);
        g.setFont(font);
        g.setColor(Color.red);
        g.drawString(Time.ThisHour(), 10, 60);
        g.drawString(heureB+" : "+minuteB+" : "+secondeB, 10, 150);
        g.drawString(heureH+" : "+minuteH+" : "+secondeH, 10, 240);
        g.drawString(heureO+" : "+minuteO+" : "+secondeO, 10, 330);
        
    }               
}