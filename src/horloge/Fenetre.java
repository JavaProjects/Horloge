/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horloge;

import g40955.util.*;
import javax.swing.JFrame;

 
public class Fenetre extends JFrame {
  
  public Fenetre(){
    this.setTitle("Horloge");
    this.setSize(900, 390);
    // Positionnement de la fenêtre au centre de l'écran
    this.setLocationRelativeTo(null);
    // true Redimensionnement possible : false Redimensionnement impossible
    this.setResizable(false);
    // true Garde toujours la fenêtre au premier plan
    this.setAlwaysOnTop(false);
    // Le programme s'éteind quand on ferme la fenêtre
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    while (true) {
        this.setContentPane(new Panneau());
        this.setVisible(true);
        Time.Interruption(0.1);
    }
  }
}